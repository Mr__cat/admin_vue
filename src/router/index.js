import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login'
import Home from '@/views/Home'
import Welcome from '@/views/welcome/Welcome'
import User from '@/views/user/User'
import Rights from '@/views/rights/Rights'
import Roles from '@/views/rights/Roles'
import Category from '@/views/category/Category'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      name: 'Home',
      component: Home,
      redirect: {path: 'welcome'},
      // 添加 根目录/ 下的子路由
      /*
        名称为children: 后面是一个数组，数组里面放入子路由的对象并且进行配置
      */
      children: [
        {
          path: 'welcome',
          name: 'Welcome',
          component: Welcome
        },
        {
          path: 'users',
          name: 'Users',
          component: User
        },
        {
          path: 'rights',
          name: 'Rights',
          component: Rights
        },
        {
          path: 'roles',
          name: 'Roles',
          component: Roles
        },
        {
          path: 'categories',
          name: 'Categrory',
          component: Category
        }
      ]
    }
  ]
})
