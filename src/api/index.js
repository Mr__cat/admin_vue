import axios from 'axios'

const baseURL = 'http://localhost:8888/api/private/v1/'

axios.defaults.baseURL = baseURL

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 将token给到一个前后台约定好的key中，作为请求发送
  let token = localStorage.getItem('mytoken')
  if (token) {
    config.headers['Authorization'] = token
  }
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

// 登录验证
export const checkUser = params => {
  return axios.post('login', params).then(res => res.data)
}
// 动态菜单获取
export const getMenus = () => {
  return axios.get('menus').then(res => res.data)
}
// 用户列表获取数据
export const getUserList = params => {
  return axios.get('users', params).then(res => res.data)
}
// 修改用户状态
export const changeUserState = params => {
  return axios.put(`users/${params.uid}/state/${params.type}`).then(res => res.data)
}
// 添加用户
export const addUser = params => {
  return axios.post('users', params).then(res => res.data)
}
// 通过ID获取用户信息
export const getUserWithId = params => {
  return axios.get(`users/${params}`).then(res => res.data)
}
// 修改用户
export const editUser = params => {
  return axios.put(`users/${params.id}`, params).then(res => res.data)
}
// 删除用户
export const deleteUser = params => {
  return axios.delete(`users/${params}`).then(res => res.data)
}
// 角色列表获取数据
export const getRolesList = params => {
  return axios.get('roles').then(res => res.data)
}
// 分配角色
export const getUserRole = params => {
  return axios.put(`users/${params.id}/role`, {id: params.id, rid: params.rid}).then(res => res.data)
}
// 权限列表获取
export const getRightList = params => {
  return axios.get(`rights/${params.type}`).then(res => res.data)
}
// 删除角色指定权限
export const deleteRoleRight = params => {
  return axios.delete(`roles/${params.roleId}/rights/${params.rightId}`).then(res => res.data)
}
// 角色授权
export const grantRoleRight = (roleId, rids) => {
  return axios.post(`roles/${roleId}/rights`, rids).then(res => res.data)
}
// 获取商品数据列表
export const getCategoriesList = (params) => {
  return axios.get('categories', {params: params}).then(res => res.data)
}
// 添加分类
export const addCategories = params => {
  return axios.post('categories', params).then(res => res.data)
}
